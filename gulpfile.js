'use strict';

const gulp = require('gulp');
const fs = require('fs');
const sass = require('gulp-sass');
const plumber = require('gulp-plumber');
const notify = require('gulp-notify');
const autoprefixer = require('gulp-autoprefixer');
const browser = require('browser-sync');
const htmlhint = require('gulp-htmlhint');
const rename = require('gulp-rename');
const postcss = require('gulp-postcss');
const uglify = require('gulp-uglify');


//ワードプレスを使わないときはこっち使う
gulp.task('server', () => {
	return browser({
		server: {
			baseDir: 'www/'
		}
	});
});

gulp.task('reload', () => {
	browser.reload();
});

//sass
gulp.task('sass', () => {
	return gulp.src('sass/**/*scss')
		.pipe(plumber({
			errorHandler: notify.onError('Error: <%= error.message %>') //<-
		}))
		.pipe(sass())
		.pipe(autoprefixer())
		.pipe(gulp.dest('www'))
		// .pipe(postcss([
		// 	require('cssnano'),
		// 	require('css-mqpacker') //media queryを1つにまとめてくれる。
		// 	]))
		// .pipe(rename({
		// 	extname:'.min.css'
		// 	}))
		// .pipe(gulp.dest('www'))
		.pipe(browser.reload({stream:true}));
});

//html 構文チェック
gulp.task('html', () => {
	return gulp.src('./www/**/*.html')
		.pipe(plumber({
			errorHandler: notify.onError('Error: <%= error.message %>')
		}))
		.pipe(htmlhint())
		.pipe(htmlhint.failReporter())
		.pipe(browser.reload({stream:true}));
});

//js圧縮
// gulp.task('uglify', () => {
// 	return gulp.src(['./www/assets/***/*.js','!./www/assets/***/*.min.js'])
// 		.pipe(uglify({preserveComments: 'some'}))
// 		.pipe(rename({
// 			extname:'.min.js'
// 			}))
// 		.pipe(gulp.dest('./www/assets/'));
// });

gulp.task('default',['server', 'sass'], function(){
	gulp.watch('sass/**/*.scss',['sass']);
		// gulp.watch('./www/**/*.html',['html']);
		// gulp.watch('./www/**/*.js',['uglify']);
		// gulp.watch('pug/**/*.pug',['pug']);
		// gulp.watch('src/**/*.js',['webpack']);
});
